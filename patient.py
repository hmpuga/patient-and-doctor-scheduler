import person as p

#Class patient inheritance from Person

class Patient(p.Person):

    def add(self, name,age,address,healthcare_plan):
        try:
            super().add(name,age)
        except Exception as e:
            print("An error occurred in the person class", e)
        else:
            try:
                p_id = super().fetch(person_name=name)
                if p_id is not None:
                    super().connect()
                    super().get_cursor.execute(
                        """INSERT INTO patients (id_person, address, healthcare_plan) VALUES (?,?,?);""",
                        (p_id[0], address, healthcare_plan,))
                    super().get_connection.commit()
                    super().close_db()
                    print("added patient sucessfully.")
                else:
                    raise Exception("An id of the patient name was not found")
            except Exception as e:
                print("An error occurred in the patient class", e)

    def update(self,name, age, address, healthcare_plan, id_person):
        try:
            super().update(name,age,id_person)
        except Exception as e:
            print("An error occurred in the person class", e)
        else:
            try:
                p_id = super().fetch(person_name=name)
                if p_id is not None:
                    super().connect()
                    super().get_cursor.execute("""UPDATE patients SET address = ?, healthcare_plan =? WHERE id_person = ?;""",
                                       (address, healthcare_plan, id_person,))
                    super().get_connection.commit()
                    super().close_db()
                    print("update patient sucessfully.")
                else:
                    raise Exception("An id of the patient name was not found")
            except Exception as e:
                print("An error occurred in the patient class", e)

    def delete(self, id):
        try:
            super().connect()
            super().get_cursor.execute("""DELETE FROM patients  WHERE id_person = ?;""", (id,))
            super().get_connection.commit()
            super().close_db()
            print("deleted patient sucessfully.")
            rsts = super().delete(id)
            if rsts == False:
                raise Exception("Delete method inside Person failed. Deleted aborted")
        except Exception as e:
            print("An error occurred:", e)

    def fetch(self, id=None, patient_name=None):
        # if Id is null (or None), then get everything, else get by id
        try:
            super().connect()
            if id is not None:
                return super().get_cursor.execute(
                    """SELECT p.name, p.age, d.address, d.healthcare_plan FROM patients d, persons p WHERE p.id = d.id_person AND id_person = ?;""",
                    (id,)).fetchall()
            elif patient_name is not None:
                return super().get_cursor.execute(
                    """SELECT d.id_person, p.name, p.age, d.address, d.healthcare_plan, d.id FROM patients d, persons p WHERE p.id = d.id_person AND p.name = ?;""",
                    (patient_name,)).fetchall()
            else:
                return super().get_cursor.execute(
                    """SELECT "ID:", p.id,"Age:" ,p.age , "Nome:", p.name, "Address:", d.address, "Plan:", d.healthcare_plan FROM patients  d, persons p WHERE p.id = d.id_person ;""").fetchall()

        except Exception as e:
            print("An error occurred", e)
        finally:
            super().close_db()

#menu to access the function of patient
def patient_options():
    print("==========================================================================")
    print("                         Crud Patient ")
    print("                   Please select one option")
    print("")
    print("1 - Add new Patient")
    print("2 - Update Patient")
    print("3 - Delete Patient")
    print("4 - List all Patients")
    print("5 - Return to main menu")
    print(" ")

#loop responsable to control the Patient's menu
def patient_menu():
    patient = Patient()
    patient_options()
    d = int(input("Select one option: "))
    while d != 5:
        print(d)
        if d == 1:
            add_patient()
        elif d == 2:
            update_patient()
        elif d == 3:
            delete_patient()
        elif d == 4:
            list_patient()
        else:
            print("Invalid option")
        patient_options()
        d = int(input("Select one option: "))

#function responsable to list all patients
def list_patient():
    patient = Patient()
    b = patient.fetch()
    for i in range(len (b)):
        print(b[i])

#function responsable to add a new patient
def add_patient():
    patient = Patient()
    name = input("Enter the name: ")
    age = input("Enter the age: ")
    address = input("Enter the address: ")
    hp = input("Enter the Healthcare Plan: ")
    patient.add(name,age,address,hp)
    print(" ")

#function responsable to update info. about one patient
def update_patient():
    name = input("Enter the Patient's name to be updated: ")
    patient = Patient()
    a = patient.fetch(None,name)
    if a == []:
        print("Patient not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            update_patient()
    else:
        name = input("Enter the new name: ")
        age = input("Enter the new age: ")
        address = input("Enter the address: ")
        hp = input("Enter the Healthcare Plan: ")
        patient.update(name, age, address,hp, a[0][0])
        print(" ")

#function responsable to delete one patient
def delete_patient():
    name = input("Enter the Patient's name to be deleted: ")
    patient = Patient()
    a = patient.fetch(None,name)
    if a == []:
        print("Patient not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            delete_patient()
    else:
        patient.delete(a[0][0])
        print(" ")
