import db_base as db
import doctor as d
import patient as p
import datetime
from datetime import date

#Class schedule inheritance from Person

class Schedule(db.DBbase):

    def __init__(self):
        super().__init__("scheduler.sqlite")

    def add(self,id_doctor,id_patient, date, time):
        try:
            super().connect()
            super().get_cursor.execute("""INSERT INTO schedules (id_doctor,id_patient, date, time) VALUES (?,?,?,?);""", (id_doctor,id_patient, date, time))
            super().get_connection.commit()
            super().close_db()
            print("added schedule sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def update(self, id_doctor, id_patient, date, time, id):
        try:
            super().connect()
            super().get_cursor.execute("""UPDATE schedules SET id_doctor = ?, id_patient = ?, date = ?, time = ? WHERE id = ?;""", (id_doctor, id_patient, date,time,id))
            super().get_connection.commit()
            super().close_db()
            print("updated schedule sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def delete(self):
        try:
            super().connect()
            super().get_cursor.execute("""DELETE FROM schedules WHERE id = ?;""", (id,))
            super().get_connection.commit()
            super().close_db()
            print("deleted schedules sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def fetch(self, id_doctor=None,id=None):
        try:
            super().connect()
            if id is not None:
                return super().get_cursor.execute("""SELECT s.id, (SELECT pe.name FROM persons pe WHERE pe.id = p.id_person) AS patient_name ,  p.healthcare_plan, s.date, s.time ,(SELECT pe.name FROM persons pe WHERE pe.id = d.id_person) AS doctor_name ,d.speciality  FROM schedules s, doctors d, patients p WHERE s.id = ? AND s.id_doctor = d.id AND s.id_patient = p.id;""", (id,)).fetchone()
            elif id_doctor is not None:
                return super().get_cursor.execute("""SELECT s.id, (SELECT pe.name FROM persons pe WHERE pe.id = p.id_person) AS patient_name ,  p.healthcare_plan, s.date, s.time ,(SELECT pe.name FROM persons pe WHERE pe.id = d.id_person) AS doctor_name ,d.speciality  FROM schedules s, doctors d, patients p WHERE s.id_doctor = ? AND s.id_doctor = d.id AND s.id_patient = p.id;""", (id,)).fetchone()
            else:
                return super().get_cursor.execute("""SELECT "ID:", s.id,"Patient:" , (SELECT pe.name FROM persons pe WHERE pe.id = p.id_person) AS patient_name , "Health Plan:" ,p.healthcare_plan,"Date:", s.date, "Time:", s.time , "Doctor Name",(SELECT pe.name FROM persons pe WHERE pe.id = d.id_person) AS doctor_name , "Speciality:", d.speciality  FROM schedules s, doctors d, patients p WHERE d.id = s.id_doctor and p.id = s.id_patient;""").fetchall()
        except Exception as e:
            print("An error occurred", e)
        finally:
            super().close_db()

    def check_schedule_day(self, date, id_doctor):
        try:
            super().connect()
            return super().get_cursor.execute("""SELECT COUNT(*) AS qtd_schdule FROM schedules WHERE date = ? AND id_doctor =?""", (date,id_doctor,)).fetchone()
        except Exception as e:
            print("An error occured", e)
        finally:
            super().close_db()

#functions to control the schdule doctor

def schedule_options():
    print("==========================================================================")
    print("                         Crud schedule ")
    print("                   Please select one option")
    print("")
    print("1 - Add new schedule")
    print("2 - Update schedule")
    print("3 - Delete schedule")
    print("4 - List all schedule")
    print("5 - Return to main menu")
    print(" ")

# Loop to control the Schedule Menu
def schedule_menu():
    schedule = Schedule()
    schedule_options()
    d = int(input("Select one option: "))
    while d != 5:
        print(d)
        if d == 1:
            add_schedule()
        elif d == 2:
            update_schedule()
        elif d == 3:
            delete_schedule()
        elif d == 4:
            list_schedule()
        else:
            print("Invalid option")
        schedule_options()
        d = int(input("Select one option: "))


def list_schedule():
    schedule = Schedule()
    b = schedule.fetch()
    print("========Schedule===========")
    for i in range(len (b)):
        print(b[i])

#Control and validate info ,in order to add a new doctor
def add_schedule():
    schedule = Schedule()
    a = check_doctor_name()
    print(a)
    if a == []:
        print("Doctor not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            add_schedule()
    else:
        b = check_patient_name()
        if b == []:
            print("Patient not found!")
            up = int(input("Press 1 to try again: "))
            if up == "1":
                add_schedule()
        else:
            date = validate_date()
            id_ = int(a[0][5])
            c = schedule.check_schedule_day(date, id_)
            w = sum(c)
            if w > 16:
                print("Doctor have too many schedules on this day, please choose another day or another Doctor!")
            else:
                time = input("Enter the time (HH:MM:SS  24HS) :")
                schedule.add(a[0][5],b[0][5],date ,time)

#function to help to validate the date
def validate_date():
    try:
        date_string = input("Enter the date (YYYY-MM-DD) :")
        date = datetime.datetime.strptime(date_string, '%Y-%m-%d')
        today = date.today()
        if date < today:
            print("You can't schedule in the past")
            validate_date()
    except ValueError:
      print("This is the incorrect date string format. It should be YYYY-MM-DD")
      validate_date()
    return date_string

#function responsible to call the class responsible to retrive the info about the doctor
def check_doctor_name():
    name = input("Enter the Doctor's name: ")
    doctor = d.Doctor()
    a = doctor.fetch(None,name)
    return a

#function responsible to call the class responsible to retrive the info about the patient
def check_patient_name():
    name = input("Enter the Patient's name: ")
    patient = p.Patient()
    a = patient.fetch(None,name)
    print(a)
    return a

#function to retrive the info of the schedule
def check_schedule_id():
    id = input("Enter the Schedule ID: ")
    schedule = Schedule()
    a = schedule.fetch(None,id)
    print(a)
    return a

#function responsible to call the class and methods responsible to update the info about the schedule
def update_schedule():
    h = check_schedule_id()
    if h == []:
        print("ID not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            update_schedule()
    else:
        schedule = Schedule()
        a = check_doctor_name()
        print(a)
        if a == []:
            print("Doctor not found!")
            up = input("Press 1 to try again: ")
            if up == "1":
                update_schedule()
        else:
            b = check_patient_name()
            if b == []:
                print("Patient not found!")
                up = int(input("Press 1 to try again: "))
                if up == "1":
                    update_schedule()
            else:
                date = validate_date()
                id_ = int(a[0][5])
                c = schedule.check_schedule_day(date, id_)
                w = sum(c)
                if w > 16:
                    print("Doctor have too many schedules on this day, please choose another day or another Doctor!")
                else:
                    time = input("Enter the time (HH:MM:SS  24HS) :")
                    schedule.update(a[0][5],b[0][5],date ,time,h[0])

#function responsible to call the class and methods responsible to delete one schedule

def delete_schedule():
    h = check_schedule_id()
    if h == []:
        print("ID not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            update_schedule()
    else:
        schedule = Schedule()
        schedule.delete(h[0])
        print(" ")
