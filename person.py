import db_base as db

##Superclass Person, with methods to persist the person object

class Person(db.DBbase):

    def __init__(self):
        super().__init__("scheduler.sqlite")

    def add(self, name, age):
        try:
            super().connect()
            super().get_cursor.execute("""INSERT INTO persons (name,age) VALUES (?,?);""", (name, age,))
            super().get_connection.commit()
            super().close_db()
            print("added person sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def update(self, name,age,id):
        try:
            super().connect()
            super().get_cursor.execute("""UPDATE persons SET name = ?, age = ? WHERE id = ?;""", (name,age,id,))
            super().get_connection.commit()
            super().close_db()
            print("updated person sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def delete(self,id):
        try:
            super().connect()
            super().get_cursor.execute("""DELETE FROM persons WHERE id = ?;""", (id,))
            super().get_connection.commit()
            super().close_db()
            print("deleted person sucessfully.")
        except Exception as e:
            print("An error occurred", e)

    def fetch(self, id=None, person_name=None):
        try:
            super().connect()
            if id is not None:
                return super().get_cursor.execute("""SELECT * FROM persons WHERE id = ?;""", (id,)).fetchone()
            elif person_name is not None:
                return super().get_cursor.execute("""SELECT id FROM persons WHERE name = ?;""", (person_name,)).fetchone()
            else:
                return super().get_cursor.execute("""SELECT * FROM persons;""").fetchall()
        except Exception as e:
            print("An error occurred", e)
        finally:
            super().close_db()

