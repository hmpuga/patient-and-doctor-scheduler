import doctor as d
import patient as p
import schedule as s

"""
Name: henrique_puga
uNID: u01317789
Propose: Patient / Doctor Scheduler - Create a patient class and a doctor class. 
 Have a doctor that can handle multiple patients and setup a scheduling program where a doctor can 
 only handle 16 patients during an 8 hr. work day.
Strategy: was created a super class called Person with attributes common both for doctor and patient, 
    created a class schedule, which contains attributes related to the appointment. 
    For tables, was created 4 tables (persons, doctors, patients, schedules) with constraints to ensure 
    the integrity and persistence in the correct way for the classes created. Also, menus were created to help the 
    data handle, validation, and visualisation.
"""

def main_menu():
    print("==========================================================================")
    print("                 Welcome to the Scheduler! ")
    print("                         Main Menu")
    print("                  Please select one option")
    print("")
    print("1 - Checking Scheduling")
    print("2 - CRUD Doctor")
    print("3 - CRUD Patient")
    print("4 - Exit")

main_menu()

n = int(input("Select one option: "))

# Loop to control Main Menu
while n != 4:
    if n == 1:
        #redirect to schedule menu
        s.schedule_menu()
    elif n == 2:
        #redirect to doctor menu
        d.doctor_menu()
    elif n == 3:
        #redirect to patient menu
        p.patient_menu()
    else:
        print("Invalid Option!!!")
    main_menu()
    n = int(input("Select one option: "))
print("Good bye!")


