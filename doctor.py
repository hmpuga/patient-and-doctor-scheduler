import person as p

#Class doctor inheritance from Person

class Doctor(p.Person):

    def add(self, name,age,speciality,description):
        try:
            super().add(name,age)
        except Exception as e:
            print("An error occurred in the person class", e)
        else:
            try:
                p_id = super().fetch(person_name=name)
                if p_id is not None:
                    super().connect()
                    super().get_cursor.execute(
                        """INSERT INTO doctors (id_person, speciality, description) VALUES (?,?,?);""",
                        (p_id[0], speciality, description,))
                    super().get_connection.commit()
                    super().close_db()
                    print("added doctor sucessfully.")
                else:
                    raise Exception("An id of the doctor name was not found")
            except Exception as e:
                print("An error occurred in the doctor class", e)

    def update(self,name, age, speciality, description, id_person):
        try:
            super().update(name,age,id_person)
        except Exception as e:
            print("An error occurred in the person class", e)
        else:
            try:
                p_id = super().fetch(person_name=name)
                if p_id is not None:
                    super().connect()
                    super().get_cursor.execute("""UPDATE doctors SET speciality = ?, description =? WHERE id_person = ?;""",
                                       (speciality, description, id_person,))
                    super().get_connection.commit()
                    super().close_db()
                    print("update doctor sucessfully.")
                else:
                    raise Exception("An id of the doctor name was not found")
            except Exception as e:
                print("An error occurred in the doctor class", e)

    def delete(self, id):
        try:
            super().connect()
            super().get_cursor.execute("""DELETE FROM doctors  WHERE id_person = ?;""", (id,))
            super().get_connection.commit()
            super().close_db()
            print("deleted doctor sucessfully.")
            rsts = super().delete(id)
            if rsts == False:
                raise Exception("Delete method inside Person failed. Deleted aborted")
        except Exception as e:
            print("An error occurred:", e)

    def fetch(self, id=None, doctor_name=None):
        # if Id is null (or None), then get everything, else get by id
        try:
            super().connect()
            if id is not None:
                return super().get_cursor.execute(
                    """SELECT p.name, p.age, d.speciality, d.description FROM doctors d, persons p WHERE p.id = d.id_person AND id_person = ?;""",
                    (id,)).fetchall()
            elif doctor_name is not None:
                return super().get_cursor.execute(
                    """SELECT d.id_person, p.name, p.age, d.speciality, d.description, d.id FROM doctors d, persons p WHERE p.id = d.id_person AND p.name = ?;""",
                    (doctor_name,)).fetchall()
            else:
                return super().get_cursor.execute(
                    """SELECT "ID:", p.id,"Age:" ,p.age , "Nome:" ,p.name,"Speciality:" , d.speciality,"Description:" ,d.description FROM doctors d, persons p WHERE p.id = d.id_person   ;""").fetchall()
        except Exception as e:
            print("An error occurred", e)
        finally:
            super().close_db()

#Menu for doctor functions

def doctor_options():
    print("==========================================================================")
    print("                         Crud Doctor ")
    print("                   Please select one option")
    print("")
    print("1 - Add new Doctor")
    print("2 - Update Doctor")
    print("3 - Delete Doctor")
    print("4 - List all Doctors")
    print("5 - Return to main menu")
    print(" ")

#Loop to control the Doctor menu
def doctor_menu():
    doctor = Doctor()
    doctor_options()
    d = int(input("Select one option: "))
    while d != 5:
        print(d)
        if d == 1:
            add_doctor()
        elif d == 2:
            update_doctor()
        elif d == 3:
            delete_doctor()
        elif d == 4:
            list_doctors()
        else:
            print("Invalid option")
        doctor_options()
        d = int(input("Select one option: "))

#function responsible to list all doctor
def list_doctors():
    doctor = Doctor()
    b = doctor.fetch()
    for i in range(len (b)):
        print(b[i])

#function responsable to add a new doctor
def add_doctor():
    doctor = Doctor()
    name = input("Enter the name: ")
    age = input("Enter the age: ")
    speciality = input("Enter the speciality: ")
    description = input("Enter the description: ")
    doctor.add(name,age,speciality,description)
    print(" ")

#function responsable to update info. about doctor
def update_doctor():
    name = input("Enter the Doctor's name to be updated: ")
    doctor = Doctor()
    a = doctor.fetch(None,name)
    if a == []:
        print("Doctor not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            update_doctor()
    else:
        name = input("Enter the new name: ")
        age = input("Enter the new age: ")
        speciality = input("Enter the new speciality: ")
        description = input("Enter the new description: ")
        doctor.update(name, age, speciality, description, a[0][0])
        print(" ")

#function responsable to delete one doctor
def delete_doctor():
    name = input("Enter the Doctor's name to be deleted: ")
    doctor = Doctor()
    a = doctor.fetch(None,name)
    if a == []:
        print("Doctor not found!")
        up = input("Press 1 to try again: ")
        if up == "1":
            delete_doctor()
    else:
        doctor.delete(a[0][0])
        print(" ")
